FROM python:3.9

WORKDIR /app

COPY . .

ENTRYPOINT ["python", "/app/hello.py"]